(* ::Package:: *)

(* ::Title:: *)
(*FermatMath*)


variableList::usage="variable list";
Imaginary::usage="To use the complex number I";


(* ::Section:: *)
(*IO*)


Options[VariablePrepare]:={Modulus->0,Imaginary->True};
VariablePrepare[varlist1_,OptionsPattern[]]:=Module[{InputFile,varlist=varlist1,str,char=OptionValue[Modulus],rep,
ImaginaryMode=OptionValue[Imaginary],varString},
	InputFile=Global`TemporaryFolder<>"fermatInput"<>ToString[$KernelID]<>".txt";
	str=OpenWrite[InputFile];
	If[char>0,WriteString[str,"&(p="<>ToString[char]<>");\n"]];
	
	(* Complex number *)
	If[ImaginaryMode,ComplexDefinition[str]];
	
	rep=Table[varlist[[i]]->ToExpression["c"<>ToString[i]],{i,1,Length[varlist]}]; 
	varString=StringJoin[Table["&(J=c"<>ToString[i]<>");\n",{i,1,Length[varlist]}]];
	WriteString[str,varString];
	
	Close[str];
	Return[InputFile];
];


ComplexDefinition[str_]:=Module[{},
			WriteString[str,"&(J=r);\n"];
			WriteString[str,"&(P=r^2+1,1);\n"];
	];


variableRep[varlist_]:=Table[varlist[[i]]->ToExpression["c"<>ToString[i]],{i,1,Length[varlist]}];
variableRev[varlist_]:=Table[ToExpression["c"<>ToString[i]]->varlist[[i]],{i,1,Length[varlist]}];


Expression2Input[expr_,rep_]:=InputForm[expr/.rep];
Expression2String[expr_,rep_]:=StringReplace[ToString[InputForm[expr/.rep]],{"I"->"r"}];
String2Expression[string_,rev_]:=ToExpression[StringReplace[string,{"r"->"I"}]]/.rev;
Imrep:={"I"->"r"};


RunFermat[InputFile_]:=Run["cat "<>InputFile<>" | "<>Global`FermatBinary];


Options[SparseArrayPrepare]:=Options[VariablePrepare];


(* Matrix should be sparse *)
SparseArrayPrepare[Matrix1_,OptionsPattern[]]:=Module[{Matrix=Matrix1,char=OptionValue[Modulus],ImaginaryMode=OptionValue[Imaginary],
rep,m,n,str,varString,AR,RowString,RowIndex,i,MatrixString="[matrix]:=[",rowList,InputFile,SparseFormat,varlist},

	AR=SplitBy[ArrayRules[Matrix],#[[1,1]]&];
	varlist=Variables[#[[2]]&/@Flatten[AR]];
	VariablePrepare[varlist,Modulus->char,Imaginary->ImaginaryMode];
	
	InputFile=Global`TemporaryFolder<>"fermatInput"<>ToString[$KernelID]<>".txt";
	str=OpenAppend[InputFile];
	m=Length[Matrix];
	n=Length[Matrix[[1]]];
	SparseFormat={"{{"->"[","}}"->"]","{"->"[","}"->"]"};
	WriteString[str,"Array matrix["<>ToString[m]<>","<>ToString[n]<>"] Sparse;\n"];
	
	rep=Table[varlist[[j]]->ToExpression["c"<>ToString[j]],{j,1,Length[varlist]}];
	
	
	For[i=1,i<=Length[AR]-1,i++,
			rowList=AR[[i]];
			RowIndex=rowList[[1,1,1]];
			RowString="["<>ToString[RowIndex]<>",";
			RowString=RowString<>StringReplace[ToString[{#[[1,2]],Expression2Input[#[[2]],rep]}&/@rowList],Join[Imrep,SparseFormat]];
			RowString=RowString<>"]";
			(* If[i!=Length[AR]-1,RowString=RowString<>"\n"];  *)(* not the last row *)
			MatrixString=MatrixString<>RowString;
	];
	WriteString[str,MatrixString<>"];\n"];
	
	
	Close[str];
	Return[{InputFile,varlist}];
]







ClearOutput[]:=DeleteFile[TemporaryFolder<>"fermatOutput"<>ToString[$KernelID]<>".txt"]Ex;


ReadExpression[file_,var_]:=Module[{result,rev},
	If[!FileExistsQ[file],Print["Expression output does not exist... Stop"];Return[Null]];
	result=Import[file,"String"];
	rev=variableRev[var];
	Return[String2Expression[result,rev]];
];


ReadOneMatrix[file_,matrixName_,var_,m_,n_]:=Module[{result,matrix,rev},
	If[!FileExistsQ[file],Print["Matrix output does not exist... Stop"];Return[Null]];
	result=Import[file,"String"];
	rev=Table[ToExpression["c"<>ToString[i]]->var[[i]],{i,1,Length[var]}]; 

	result=StringReplace[result,{":="->"->",matrixName<>"["~~Shortest[x__]~~"]":>"{"<>x<>"}",";"->","}];
	result="{"<>StringDrop[result,-1]<>"Nothing}";
	
	matrix=SparseArray[String2Expression[result,rev],{m,n}];
	Return[matrix];
	
]


(* ::Section:: *)
(*Polynomial and Rational function*)


Options[FermatEval]:=Options[VariablePrepare];


(* Matrix should be sparse *)


FermatEval[expr1_,OptionsPattern[]]:=Module[{expr=expr1,varlist,char=OptionValue[Modulus],ImaginaryMode=OptionValue[Imaginary],InputFile,OutputFile,str,InputString,rep},
	varlist=Variables[expr];
	InputFile=VariablePrepare[varlist,Modulus->char,Imaginary->ImaginaryMode];

	OutputFile=Global`TemporaryFolder<>"fermatOutput"<>ToString[$KernelID]<>".txt";
	rep=Table[varlist[[i]]->ToExpression["c"<>ToString[i]],{i,1,Length[varlist]}]; 
	str=OpenAppend[InputFile];
	
	WriteString[str,"&(S='"<>OutputFile<>"');\n"];
	WriteString[str,"!(&o,"<>Expression2String[expr,rep]<>");\n"];
	WriteString[str,"&q;"];
	Close[str];
	RunFermat[InputFile];
	Return[ReadExpression[OutputFile,varlist]];	
]


(* ::Section:: *)
(*Linear algebra*)


Options[FermatSparseRowEchelon]:=Join[Options[VariablePrepare],{Timing->False}];


(* Matrix should be sparse *)
FermatSparseRowEchelon[Matrix1_,OptionsPattern[]]:=Module[{Matrix=Matrix1,varlist,char=OptionValue[Modulus],m,n,
str,OutputFile,InputFile,TimingMode=OptionValue[Timing],timer,ImaginaryMode=OptionValue[Imaginary]},
	If[TimingMode,timer=AbsoluteTime[]];
	OutputFile=TemporaryFolder<>"fermatOutput"<>ToString[$KernelID]<>".txt";
	{InputFile,varlist}=SparseArrayPrepare[Matrix,Modulus->char,Imaginary->ImaginaryMode];
	ClearOutput[];
	m=Length[Matrix];
	n=Length[Matrix[[1]]];
	str=OpenAppend[InputFile];
	WriteString[str,"Redrowech([matrix]);\n"];
	WriteString[str,"&(S='"<>OutputFile<>"');\n"];
	WriteString[str,"!(&o,[matrix]);\n"];

	WriteString[str,"&q;"];
	Close[str];
	If[TimingMode,Print["Disk writing time... ",AbsoluteTime[]-timer]; timer=AbsoluteTime[]];
	RunFermat[InputFile];
	If[TimingMode,Print["fermat rowechlon time... ",AbsoluteTime[]-timer]; timer=AbsoluteTime[]];
	Matrix=ReadOneMatrix[OutputFile,"matrix",varlist,m,n];
	If[TimingMode,Print["Disk reading time... ",AbsoluteTime[]-timer]; timer=AbsoluteTime[]];
	Return[Matrix];
];


Options[FermatDet]:=Options[VariablePrepare];


(* Matrix should be sparse *)


FermatDet[Matrix1_,OptionsPattern[]]:=Module[{Matrix=Matrix1,varlist,char=OptionValue[Modulus],
str,OutputFile,InputFile,ImaginaryMode=OptionValue[Imaginary]},
	
	OutputFile=TemporaryFolder<>"fermatOutput"<>ToString[$KernelID]<>".txt";
	{InputFile,varlist}=SparseArrayPrepare[Matrix,Modulus->char,Imaginary->ImaginaryMode];
	ClearOutput[];
	
	str=OpenAppend[InputFile];
	
	WriteString[str,"&(S='"<>OutputFile<>"');\n"];
	WriteString[str,"!(&o,Det[matrix]);\n"];
	WriteString[str,"&q;"];
	Close[str];
	
	RunFermat[InputFile];

	Return[ReadExpression[OutputFile,varlist]];
];
