FermatMath

It is a simple package which serves as an interface between Mathematica and Fermat. It writes Mathematica expressions on disk and calls Fermat to do various computations.